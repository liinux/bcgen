#!/usr/bin/python
"""Proof of concept for the Bulls and Cows solution tree"""
from __future__ import print_function

import random

from bcgen import generate_tree, getcode, createresult, strtuple


if __name__ == '__main__':
	# Set a random secret code
	secret = tuple(random.sample(range(10), 4))

	# Generate the tree 
	tree = generate_tree()

	# Allow ten tries to guess the code
	tries = 10

	# Start guessing
	results = []

	for _ in range(tries):
		code = getcode(tree, results)
		result = createresult(secret, code)
		results.append(result)
		print('{}, {} bulls {} cows'.format(strtuple(code), result.bulls, result.cows))

		if result.bulls == 4:
			print('\nWin in {} moves'.format(len(results)))
			break
	else:
		print('\nLoss, the correct code was {}'.format(strtuple(secret)))


#!/usr/bin/python
"""A naive generator for Bulls and Cows solution trees
Average moves needed: 5.5-5.6
Maximum moves needed: 8-10
"""
from __future__ import print_function

import itertools
import random
import collections


# Data classes
BullsCowsResult = collections.namedtuple('Result', ('bulls', 'cows'))
BullsCowsTreebranch = collections.namedtuple('Branch', ('result', 'code', 'subbranches', 'layer'))


def createresult(answer, guess):
	"""Compare a code to the answer and create a result"""
	bulls = 0
	cows = 0

	for a, g in zip(answer, guess):
		if g == a:
			bulls += 1
		elif g in answer:
			cows += 1

	return BullsCowsResult(bulls, cows)


def strtuple(tuple_):
	"""Convert a tuple to string"""
	return ''.join([str(c) for c in tuple_])


def strtree(branch):
	"""Convert a tree branch and its subbranches to string representation recursively"""
	indent = branch.layer-1
	output = '\t' * indent + strtuple(branch.result) + ' ' + strtuple(branch.code) + '\n'

	subbranches = branch.subbranches.values()
	subbranches.sort(key=lambda d : str(d.result[0]) + str(d.result[1]))

	for subbranch in subbranches:
		output += strtree(subbranch)

	return output


def generate_tree(rootcode=None):
	"""Generate a full Bulls and Cows tree, using a random code or specified code for the tree root"""
	# All possible codes in Bulls and Cows
	codes = list(itertools.permutations(range(10), 4))

	# Create root branch
	if not rootcode:
		rootcode = random.choice(codes)

	treeroot = BullsCowsTreebranch(BullsCowsResult(4, 0), rootcode, {}, 1)

	# Insert all remaining codes into the tree
	codes.remove(rootcode)

	for code in codes:
		layer = 2
		parent = treeroot
		result = createresult(parent.code, code)

		while result in parent.subbranches:
			layer += 1
			parent = parent.subbranches[result]
			result = createresult(parent.code, code)

		branch = BullsCowsTreebranch(result, code, {}, layer)
		parent.subbranches[result] = branch

	return treeroot


def getcode(treeroot, results):
	"""Look next code up from a tree using previous results"""
	branch = treeroot

	for result in results:
		branch = branch.subbranches[result]

	return branch.code


def main():
	"""Generate a tree and print it"""
	treeroot = generate_tree()
	tree_string = strtree(treeroot)[:-1]
	print(tree_string)


if __name__ == '__main__':
	main()


Author: Robert Tiinus

A naive and trivial Bulls and Cows solution tree generator.  
Tree generator is in bcgen.py, run bcpoc.py for proof-of-concept.

A big thank you to Alexey Slovesnov, I read your paper about optimal Bulls and Cows algorithms and it inspired me to create this very suboptimal but quick algorithm.

